package com.example.ud3_ejemplo8

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        // "Inflamos" el menú diseñado
        menuInflater.inflate(R.menu.menu_main, menu)

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Dependiendo del elemento pulsado hacemos una acción u otra.
        return when(item.itemId){
            R.id.elem1 -> {
                Toast.makeText(this, "Elemento 1", Toast.LENGTH_SHORT).show()
                true
            }
            R.id.elem2 -> {
                Toast.makeText(this, "Elemento 2", Toast.LENGTH_SHORT).show()
                true
            }
            R.id.elem3 -> {
                Toast.makeText(this, "Elemento 3", Toast.LENGTH_SHORT).show()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}